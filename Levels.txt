{
	"Locations" : 
	[
		{"Id" : 1, 
		"Name" : "House",
		"Description" : "This is the first location", 
		"ConnectedLocations" :[2] 
		},
		{"Id" : 2, 		
		"Name" : "Forest",
		"Description" : "This is the second location",
		"ConnectedLocations" : []}
	]
}