#pragma once
#include "StateBase.h"
#include "Location.h"


class GameLogic :
	public StateBase
{
private:
	GameLogic() {};
public:
	static GameLogic* GetInstance();
	void Show(StateManager* context, Location* level);
};