#include "StateBase.h"

void StateBase::Show(StateManager* context)
{
	cout << "Press ESC or Enter to return to the Main Menu" << endl;
	cin.clear();
	cin.ignore(1, '\n');
	int input = _getch();

	if (input == 27 ||
		input == 10 ||
		input == 13)
	{
		context->ChangeState(0);
	}
}