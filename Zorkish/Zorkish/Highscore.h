#pragma once
#include "StateBase.h"
class Highscore :
	public StateBase
{
private:
	Highscore() {};

public:
	static Highscore* GetInstance();
	void Show(StateManager* context);
};