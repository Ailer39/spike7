#include "Location.h"

void Location::AddLocation(Location* newLocation)
{
	_connectedNotes->insert(_connectedNotes->end(), newLocation);
}

Location* Location::GetLocation(int direction)
{
	if (_connectedNotes->size() >= direction)
	{
		_connectedNotes->at(direction);
	}

	return nullptr;
}

void Location::AddGameObject(GameObject* gameObj)
{

}

void Location::RemoveGameObject(GameObject* gameObj)
{

}

string Location::GetName()
{
	return _name;
}

int Location::GetId()
{
	return _id;
}

string Location::GetDescription()
{
	return _description;
}

void Location::PrintConnections()
{
	cout << "Connected Levels: " << endl;

	for (unsigned int i = 0; i < _connectedNotes->size(); i++)
	{

		cout << " (" << i << ") " << _connectedNotes->at(i)->GetName() << endl;
	}
}

Location::Location(int id, string name, string description)
{

	_name = name;
	_id = id;
	_description = description;
	_connectedNotes = new vector<Location*>();
}