#include "LevelReader.h"

Location* LevelReader::GetLevel()
{
	Location* result = nullptr;
	return result;
}

string LevelReader::ReadFile()
{
	ifstream fileStream;
	fileStream.open(_path);

	if (fileStream.is_open())
	{
		stringstream tmpStream;
		tmpStream << fileStream.rdbuf();
		return tmpStream.str();
	}

	return "";
}

vector<int> LevelReader::GetConnections()
{
	vector<int> result;

	return result;
}

Location* LevelReader::FindLocationById(int id)
{
	for (int i = 0; i < _locations.size(); i++)
	{
		if (_locations[i]->GetId() == id)
		{
			return _locations[i];
		}
	}

	return nullptr;
}

vector<Location*> LevelReader::GetLevelGraph()
{
	_locations.clear();
	string levelText = ReadFile();
	Document doc;
	doc.Parse(levelText.c_str());

	if (doc["Locations"].IsArray())
	{
		const Value& jsonSrc = doc["Locations"];
		// Create locations
		for (int i = 0; i < jsonSrc.Size(); i++)
		{
			_locations.insert(_locations.end(), new Location(jsonSrc[i]["Id"].GetInt(),
				jsonSrc[i]["Name"].GetString(),
				jsonSrc[i]["Description"].GetString()));
		}

		// Add connections 
		for (int i = 0; i < jsonSrc.Size(); i++)
		{
			if (jsonSrc[i]["ConnectedLocations"].IsArray())
			{
				const Value& connections = jsonSrc[i]["ConnectedLocations"];
				for (int n = 0; n < connections.Size(); n++)
				{
					Location* src = FindLocationById(jsonSrc[i]["Id"].GetInt());
					Location* connectedElement = FindLocationById(connections[n].GetInt());

					if (src != nullptr &&
						connectedElement != nullptr)
					{
						if (connectedElement != nullptr)
						{
							src->AddLocation(connectedElement);
						}
					}
				}
			}
		}
	}

	return _locations;
}

LevelReader::LevelReader(string path)
{
	_path = path;
}