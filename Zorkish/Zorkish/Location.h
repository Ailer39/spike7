#pragma once

#include <string>
#include <vector>
#include <iostream>
#include "GameObject.h"
#include "Directions.h"

using namespace std;

class Location
{
private:
	vector<Location*>* _connectedNotes;
	vector<GameObject*> _gameObjects;
	string _name;
	string _description;
	int _id;
public:
	Location(int id, string name, string description);
	void AddLocation(Location* newLocation);
	void AddGameObject(GameObject* gameObj);
	void RemoveGameObject(GameObject* gameObj);
	Location* GetLocation(int direction);
	string GetName();
	string GetDescription();
	int GetId();
	void PrintConnections();
};