#include "GameLogic.h"

static GameLogic* instance;

GameLogic* GameLogic::GetInstance()
{
	if (instance == nullptr)
	{
		instance = new GameLogic();
	}

	return instance;
}

void GameLogic::Show(StateManager* context, Location* level)
{
	cout << level->GetDescription() << endl;
	level->PrintConnections();
	cout << "Go to level: _" << endl;

	unsigned int connectionNumber;
	cin >> connectionNumber;

	Location* nextLevel = level->GetLocation(connectionNumber);
	if (nextLevel != nullptr)
	{
		Show(context, nextLevel);
	}
	else
	{
		context->ChangeState(0);
	}
}