#include "Node.h"

void Node::AddNote(Node* newNode)
{
	_connectedNotes->insert(_connectedNotes->end(), newNode);
}

Node* Node::GetNode(unsigned int connectionNumber)
{
	if (_connectedNotes->size() > connectionNumber)
	{
		return _connectedNotes->at(connectionNumber);
	}

	return nullptr;
}

string Node::GetName()
{
	return _name;
}

int Node::GetId()
{
	return _id;
}

void Node::PrintConnections()
{
	cout << "Connected Levels: " << endl;

	for (unsigned int i = 0; i < _connectedNotes->size(); i++)
	{
		cout << " (" << i << ") " << _connectedNotes->at(i)->GetName() << endl;
	}
}

Node::Node(int id, string name)
{
	_name = name;
	_id = id;
	_connectedNotes = new vector<Node*>();
}