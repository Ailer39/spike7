#pragma once
#include <vector>
#include <map>
#include <string>
#include <fstream>
#include <sstream>	
#include "Node.h"

#include <iostream>


using namespace std;

class LevelReader
{
private:
	static Node* GetLevel(string levelText);
	static vector<int> GetConnections(string LevelText);
	static Node* FindNodeById(vector<Node*> nodes, int id);
public:
	static vector<Node*> GetLevelGraph(string path);
};