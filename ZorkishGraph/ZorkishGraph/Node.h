#pragma once

#include <string>
#include <vector>
#include <iostream>

using namespace std;

class Node
{
private:
	vector<Node*>* _connectedNotes;
	string _name;
	int _id;
public:
	Node(int id, string name);
	void AddNote(Node* newNode);
	Node* GetNode(unsigned int connectionNumber);
	string GetName();
	int GetId();
	void PrintConnections();
};