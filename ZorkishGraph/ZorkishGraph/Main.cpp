#include <iostream>
#include <vector>
#include "Node.h"
#include "LevelReader.h"

using namespace std;

int main()
{

	Node* currentLevel = nullptr;

	int nextLevelInput = -1;
	string path;
	cout << "Enter path to level file: " << endl;
	cin >> path;

	vector<Node*> levels = LevelReader::GetLevelGraph(path);

	if (levels.size() == 0)
	{
		cout << "No level available";
		return -1;
	}

	currentLevel = levels.at(0);
	

	while (true)
	{
		cout << "Current Level: " << currentLevel->GetName() << endl;
		cout << "Possible connections: " << endl;
		currentLevel->PrintConnections();
		cout << "Insert connection number:_ " ;
		cin >> nextLevelInput;
		Node* tmp = currentLevel->GetNode(nextLevelInput);

		if (tmp != nullptr)
		{
			currentLevel = tmp;	
		}
	}

	system("pause");
}