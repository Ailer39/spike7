#include "LevelReader.h"

Node* LevelReader::GetLevel(string levelText)
{
	istringstream stream(levelText);
	string content;
	vector <string> levelContent;
	Node* result = nullptr;

	while (getline(stream, content, ';'))
	{
		levelContent.insert(levelContent.end(), content);
	}

	if (levelContent.size() > 1)
	{
		result = new Node(stoi(levelContent[0]), levelContent[1]);
	}

	return result;
}

vector<int> LevelReader::GetConnections(string levelText)
{
	istringstream stream(levelText);
	string content;
	vector <string> connections;
	vector<int> result;
	int counter = 0;

	while (getline(stream, content, ';'))
	{
		if (counter > 1)
		{
			result.insert(result.end(), stoi(content));
		}

		counter += 1;
	}

	return result;
}

Node* LevelReader::FindNodeById(vector<Node*> nodes, int id)
{
	for (int i = 0; i < nodes.size(); i++)
	{
		if (nodes[i]->GetId() == id)
		{
			return nodes[i];
		}
	}

	return nullptr;
}

vector<Node*> LevelReader::GetLevelGraph(string path)
{
	vector<Node*> result;
	ifstream fileStream;
	fileStream.open(path);

	int id;

	if (fileStream.is_open())
	{
		string fileContent;
		string id;
		map<int, string> levelDescriptions;
		Node* tmpNode = nullptr;

		while (!fileStream.eof())
		{
			getline(fileStream, fileContent);
			tmpNode = GetLevel(fileContent);
			result.insert(result.end(), tmpNode);
			levelDescriptions.insert(pair<int, string>(tmpNode->GetId(), fileContent));
		}

		vector<int> connections;
		for (int i = 0; i < result.size(); i++)
		{
			connections = GetConnections(levelDescriptions[result[i]->GetId()]);
			for (int n = 0; n < connections.size(); n++)
			{
				tmpNode = FindNodeById(result, connections[n]);

				if (tmpNode != nullptr)
				{
					result[i]->AddNote(tmpNode);
				}
			}
		}
	}

	return result;
}